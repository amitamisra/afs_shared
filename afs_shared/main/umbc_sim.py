import re

import requests


class UmbcPageDownException(Exception):
    pass


class UmbcSimilarity:
    UMBC_STS_HOST = "http://swoogle.umbc.edu"
    UMBC_STS_PATH = UMBC_STS_HOST + "/StsService/GetStsSim"
    UMBC_TOP_N_PATH = UMBC_STS_HOST + "/SimService/GetSimilarity"

    # Pattern to extract useful piece of the response for topN
    TOP_N_RE = re.compile(r".*<textarea.*>\n(.*)</textarea>.*")

    @staticmethod
    def get_sim(text_1, text_2):
        """
        Gets the umbc similarity for two texts
        :param text_1:
        :param text_2:
        :return: Umbc sim as a float
        """
        payload = {
            "operation": "api",
            "phrase1": text_1,
            "phrase2": text_2,
        }
        r = requests.get(UmbcSimilarity.UMBC_STS_PATH, params=payload)
        r.raise_for_status()
        return float(r.text)

    @staticmethod
    def get_top_n(word, pos, n, concept, corpus):
        if pos not in {"NN", "VB", "JJ", "RB"}:
            raise ValueError("pos must be one of the following: NN, VB, JJ, RB")
        if corpus not in {"webbase", "gigawords"}:
            raise ValueError("corpus must be one of the following: webbase, gigawords")
        if concept not in {"concept", "relation"}:
            raise ValueError("concept must be one of the following: concept, relation")
        payload = {
            "operation": "top_sim",
            "word": word,
            "pos": pos,
            "N": n,
            "sim_type": concept,
            "corpus": corpus,
        }
        r = requests.get(UmbcSimilarity.UMBC_TOP_N_PATH, params=payload)
        r.raise_for_status()
        groups = UmbcSimilarity.TOP_N_RE.findall(r.text)
        if len(groups) == 0:
            # Pattern didn't match. This either means the format of the results changed (unlikely), or their
            # service is overloaded and returning blank pages (likely).
            raise UmbcPageDownException("Error: Failed to find the "
                                        "textarea in the umbc result for topN."
                                        " This may be do to the output format changing or a service "
                                        "outage on their end.")
        results = list()
        # Parse results into tuples
        for word_pos in groups[0].split(","):
            if "_" in word_pos:
                tup = word_pos.split("_")
                word, pos = tup[0].strip(), tup[1].strip().split(" ")[0]
                results.append((word, pos))

        return results
