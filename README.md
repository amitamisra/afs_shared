# README #

Project repo for AFS_shared 2016. 

Natural Language and Dialogue Systems Lab, UCSC
It contains the code for the paper
@conference{misraetal2016SIGDIAL,
title = {Measuring the Similarity of Sentential Arguments in Dialogue},
booktitle = {Proceedings of the SIGDIAL 2016 Conference},
year = {2016},
month = {September},
publisher = {Association for Computational Linguistics},
organization = {Association for Computational Linguistics},
author = {Misra,Amita and Ecker,Brian and  Walker,Marilyn}
}

This is refactored code. Original was in java. I ported it to python so that it can be easily reused.
Hence the results are close( all features results are better in this version) but not exactly same as this uses a different version of  corenlp.
The results on executing this code are in file afs_data/results/afs_results.csv


Requirements:
use python3
run java corenlp as a server, it uses py4j

install corenlp

pyrouge
https://pypi.python.org/pypi/pyrouge/0.1.0
in site-packages in
pyrouge change the file

In resources folder add LIWC dictionary in liwc/LIWC2007.dic

utils/log.py
set mode to WARNING

This is refactored code for afs_2016 in python.

It requires a Java CoreNLP server running.
1)Run the program  SimpleCoreNLP_3_5_2.java from repo corenlp_sts. It is modified to run for AFS as token return indexes are off by 1, returns token index starting from 0 instead of 1.
    This behaviour is different from normalcorenlp.
2) Run main/buildfeatures
This creates features for each of the three topics. Settings to run are specified in config/buildfeatures_Config.ini
You need to specify the location of the input file and coresponding feature file and all the resources required.
Feature generation takes time as for every row, we make a call to java server. For 1800 instances for DP it took around 1580 sec and for gm 2215sec.

3)Run main/runregression.py
It takes input from regression.ini
csv_with_file_list is a file that is used to run SVM and ridge regression. You can specify the 
train, test file, the type of classifier( ridge/SVM) ;  eval type(CV/test); done(0/1) if 1 then skip the current row.
SVM with word2vec and all features is very slow due to nested CV and grid search, but ridge is fast. SVM without word2vec is quite fast.
 

